; Configuration file for EMW ULTIMATE XL. RTFM! https://duet3d.dozuki.com/Wiki/Gcode#main
; Edited by Aaron Peterson for his Ultimate XL

;----- GENERAL -----

M111 S0                            		        ; set debug level to off
; G21						                        ; set uom to millimetres                            If this works while commented just delete the line
G90						                        ; set duet to send absolute coordinates
M83						                        ; set duet to send relative extruder moves

;----- NETWORKING -----

M550 P"ULTIMATEXL"					            ; set machine name and netbios name (mDNS)
M551 P"reprap"                   			    ; set machine password (used for FTP). Sent in cleartext. 
M552 S1						                    ; enable networking interface
; M555 P2						                    ; set duet input/output to look like Marlin.        If this works while commented just delete the line

;----- DRIVES -----

M584 X0 Y1:4 Z2 E3							    ; drive mapping. X0 IS x axis mapped to drive 0
M569 P0 S1                                      ; drive 0 direction - x axis - S1 (forwards) OR S0 (backwards)
M569 P1 S0                                      ; drive 1 direction - y axis - motor 1 - S1 (forwards) OR S0 (backwards)
M569 P2 S0                                      ; drive 2 direction - z axis - S1 (forwards) OR S0 (backwards)
M569 P3 S0  				                    ; drive 3 direction - extruder - S1 (forwards) OR S0 (backwards)
M569 P4 S1                                      ; drive 4 direction - y axis - motor 2 - S1 (forwards) OR S0 (backwards)
M350 X16 Y16 Z16 E16 I1                         ; set all drivers t0 x16 microstepping. enable microstep interpolation to x256 stepping (I1)
M92 X200 Y200 Z400 E409                         ; set steps per mm  # TODO calibrate extruder steps. set to the nominal steps the internet provided.
M201 X2800 Y2800 Z3500 E3500	                ; set max acceleration in mm/s
M203 X20000 Y20000 Z3500 E5000				    ; set max speed (feedrate) in mm/m
M566 X700 Y700 Z700 E800					    ; set max instantaneous speed change (jerk) in mm/s
M906 X1600 Y1600:1600 Z1400 E1330 I30            ; set motor current in mA. I = idle current %. last value E800
M84 S30                                         ; motor current idle timeout in seconds

;----- AXIS LIMITS -----

M208 X-29 Y-22 Z-0.5 S1                         ; set min axis travel # default: X-18 Y-22 Z-0.5 S1
M208 X295 Y295 Z289.60 S0                       ; set max axis travel # Z MAX VALUE DURING INTIAL CALIBRATION IS TUNED HERE  290 Z291.53 #default

;----- ENDSTOPS -----

M574 X1 Y1 S1                                   ; set active low endstops
M574 Z2 S1                                      ; set active high endstops

;----- LINEAR ADVANCE -----

M572 D0 S0.04 		                            ; set extruder 0 'pressure advance' to 0.04 seconds

;----- Z PROBE -----

; BLTouch
;M98 P"bltouch_config.g"
M558 P9 H2 F100 T15000                          ; define bltouch probe
;G31 X38 Y10 Z2.95 P600                         ; old probe offset
G31 X-23.8 Y0 Z1.95 P600                        ; set probe offset. default P25. last value: 2.95. raising Z value brings the nozzle closer to the bed.

; metrol probe
;M558 P4 F280 H2 I1 T15000 A4 S0.01	            ; M558 SET Z PROBE TYPE. TODO: adjust H value to minimize probe travel
;G31 X0 Y0 Z70.97 P5				            ; G31 SET Z PROBE OFFSETS. ADJUST THIS Z VALUE FOR CALIBRATING PROBE HEIGHT

; Mesh Probe
M557 X0:290 Y0:290 S29						    ; orig: define G29 probing actions. define probe mesh grid
;M557 X10:290 Y20:290 S28						; new: define G29 probing actions. define probe mesh grid. S is grid point spacing



;----- PROBE COMPENSATION TAPER -----

;M376 H10									    ; taper height for grid compensation. slowly taper compensation up to a set height. once at height, compensation will not be in effect. DISABLED BY DEFAULT (EMW ULXL)

;TEMPERATURE SENSOR PARAMETERS -----

M305 P0 T100000 B3950 R2200 H0 L0		        ; set temp sensor parameters. bed sensor is a 100k thermistor with a beta value of 3950. duet maestro inline resistor = 2200
M305 P1 T100000	B4725 C0.0000000706 R2200 H0 L0 ; set temp sensor parameters. hot end sensor is a 100k thermistor with a beta value of 4725. duet maestro inline resistor = 2200. c value is compensation value at higher temps

;----- HEATER TUNING PID -----

M307 H0 A522.8 C1351.1 D19.3 B0                 ; set heating process parameters. H0 is the heat bed. autotuned November 2019 EMW ULXL  
M307 H1 A443.4 C239.5 D3.9 V24.1 B0             ; autotuned results for stock e3d volcano. 05/2020 ajp. M307 H1 to see the details of the last run. 

;----- HEATER SAFETY -----

M143 H0 S105 A1                                 ; set max bed heat temperature
M143 H1 S305 A1                                 ; set max hot end temperature

;----- FANS -----

M106 P0 S-1 L0.2                                ; set fan 0 value, pwm signal inversion and frequency. thermostatic control is turned off
;M106 P0 S-1 L255 C"Air Pump"                                ; set any fan speed to max for the air pump relay
M106 P1 S1.0 F500 H1 T45                        ; set fan 1 value, pwm signal inversion and frequency. thermostatic control is turned on
M106 P2 H100:101 T35:45 L0.6                    ; set fan 2 value, pwm signal inversion and frequency. thermostatic control is turned on

;----- TOOLS -----

M563 P0 D0 H1                                   ; define tool 0 as extruder 0 using heater 1
G10 P0 X0 Y0 Z0                                 ; set tool 0 axis offsets
G10 P0 R0 S0                                    ; set initial tool 0 active and standby temperatures to 0C
T0                                              ; activate tool 0

;----- POWER RECORVERY -----

M911 S23.5 R23.9 P"M913 X0 Y0 G91 M83 G1 Z3 E-1 F1000" ; POWER FAILURE RECOVERY. EMW ULXL REQUIRES SEPERATE MODULE TO WORK. PLEASE CONTACT ELITE MACHINE WORKS FOR DETAILS.

;----- CUSTOM SETTINGS -----

M80                                             ; atx power on via psu control pin. used to control bed heater relay. bed heater relay is activated when powering on or rebooting printer.