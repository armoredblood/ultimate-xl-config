;https://betrue3d.dk/bltouch-on-duet-wifi-configuratio-and-usage/

G29 S2                  ; clear height map
G28                     ; home all axes
G4 P0                   ; dwell for 0 ms
G29 S0                  ; mesh bed probe, save heightmap.csv, activate bed compensation
G4 P0                   ; dwell for 0 ms
G29 S3                  ; save height map
G4 P0                   ; dwell for 0 ms
G28                     ; home all axes
G90                     ; set to absolute positioning
G0 X150                 ; move to X150
