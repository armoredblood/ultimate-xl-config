; stop.g
; called when M0 (Stop) is run (e.g. when a print from SD card is cancelled)

G4 P0 ; wait
M221 S100
M104 S0 ; turn off temperature
M140 S0 ; turn off heatbed
M107 ; turn off fan
G91 G0 Z20
G90 G0 X-27 Y295