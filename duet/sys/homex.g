G91                     ; relative mode
G1 Z4 S1 F200           ; raise head to avoid dragging nozzle over the bed
G1 X-330 F3000 S1       ; move up to 330mm in the -X direction, stopping if the homing switch is triggered
G1 X4 F600              ; move slowly 4mm in the +X direction
G1 X-10 S1              ; move slowly 10mm in the -X direction, stopping at the homing switch
G1 X29 F3000            ; move 29mm in the +X direction ending at X0
G1 Z-4 S2 F200          ; lower the head again
G90                     ; back to absolute mode