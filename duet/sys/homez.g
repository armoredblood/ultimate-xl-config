G91                     ; relative mode
G1 Z5 F800 S2           ; lift z so we don't crash
G90                     ; absolute mode
;G1 X150 Y150 F6000     ; move to the center of the bed
;G1 X-27 Y25 F6000       ; move to bottom left corner
G1 X25 Y25 F6000       ; move to bottom left corner
M558 F500               ; bring the probe down quickly, will be less accurate.
G30                     ; single bed probe
G4 P250                 ; dwell for 250 ms
M558 F50                ; probe again slowly for precision
G30                     ; single bed probe
G1 X0 Y0 Z25 F2000

